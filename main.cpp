#include <chrono>
#include <cmath>
#include <condition_variable>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <thread>
#include <vector>

#include <Leap.h>
#include <UltrahapticsVector3.hpp>
#include <UltrahapticsUnits.hpp>
#include <UltrahapticsAlignment.hpp>
#include <UltrahapticsTimePointStreaming.hpp>

#include "arduino_serial_lib.h"
#include "keyboard.h"
#include "latin_square.h"
#include "logger.h"



//---------------------------------------------------------------------------------------------------------
//
// Create aliases for class names to make them shorter

typedef Ultrahaptics::Vector3 Point3D;
typedef Ultrahaptics::TimePointStreaming::OutputInterval::iterator IntervalIterator;
typedef Ultrahaptics::TimePointStreaming::OutputInterval OutputInterval;
typedef Ultrahaptics::TimePointStreaming::Emitter TimePointEmitter;



//---------------------------------------------------------------------------------------------------------
//
// Constants defining the feedback parameters

#define CONE_MAX_RADIUS (2.0f * (float) Ultrahaptics::Units::centimetres)            // Max radius of the haptic feedback circle
#define CONE_TRAVERSAL_FREQUENCY (75.0f * (float) Ultrahaptics::Units::hertz)        // Haptic feedback circle rendered at 75Hz
#define INTERACTION_SPHERE_RADIUS (12.0f * (float) Ultrahaptics::Units::centimetres) // Radius of the `sensor strength' sphere
#define SWEET_SPOT_STRENGTH_BOOST 0.05f                                               // Add to sensor strength to widen the sweet spot
#define LED_UPDATE_STEP_THRESHOLD 0.05 // LEDs only update if the sensor strength changes by this much, reduces serial IO
#define LOW_STRENGTH_R 50
#define LOW_STRENGTH_G 50              // Low sensor strength feedback LED RGB values
#define LOW_STRENGTH_B 50
#define HIGH_STRENGTH_R 0
#define HIGH_STRENGTH_G 200            // High sensor strength feedback LED RGB values
#define HIGH_STRENGTH_B 0



//---------------------------------------------------------------------------------------------------------
//
// Experiment-specific code and constants

#define CONDITION_VISUAL 1
#define CONDITION_HAPTIC 2
#define CONDITION_MULTI 3
#define N_BLOCKS 6
#define N_TASKS_PER_BLOCK 20
#define SPACEBAR_PRESS_THRESHOLD_TIME 500
#define PAUSE_BETWEEN_TASKS 1000
#define DEMO_PARTICIPANT 99
#define DEMO_BLOCK 2 // To give the HL condition

// Coordinate space where random points will be selected
#define MIN_X -4
#define MAX_X -(MIN_X)
#define MIN_Y -4
#define MAX_Y -(MIN_Y)
#define MIN_Z 14
#define MAX_Z 20

std::mutex mutex;
std::condition_variable next_task_ready;

/**
 * Returns a string representation of the condition name.
 */
static std::string getConditionName(int condition) {
    switch ((condition % 3) + 1) {
        case CONDITION_VISUAL:
            return "L";
        case CONDITION_HAPTIC:
            return "H";
        case CONDITION_MULTI:
            return "HL";
        default:
            return "";
    }
}

/**
 * Constructs a line for the log file using the given data. Automatically appends a header before the first task line.
 */
static std::string getLogLine(int participant, int block, int condition, int task, double task_time, double distance, Point3D target, Point3D hand_final, double strength) {
    std::stringstream line;

    if (task == 1) {
        line << "Participant,Block,Condition,ConditionName,Task,Time(ms),Distance(mm),Target.x,Target.y,Target.z,Hand.x,Hand.y,Hand.z,Strength" << std::endl;
    }

    std::string condition_name = getConditionName(condition);

    line << participant << "," << block << "," << condition << "," << condition_name << "," << task << ",";
    line << task_time << "," << distance * 1000.0 << ",";
    line << target.x * 1000.0 << "," << target.y * 1000.0 << "," << target.z * 1000.0 << ",";
    line << hand_final.x * 1000.0 << "," << hand_final.y * 1000.0 << "," << hand_final.z * 1000.0 << ",";
    line << strength;
    line << std::endl;
    return line.str();
}

/**
 * Constructs the log file name for the given participant and block combination.
 */
static std::string getLogName(int participant, int block) {
    std::stringstream name;
    name << "P" << participant << "-" << "B" << block << ".csv";
    return name.str();
}

/**
 * Generates a random point within the acceptable range of target points.
 */
static Point3D getRandomPoint() {
    float x = MIN_X + static_cast<float> (rand()) / (static_cast<float> (RAND_MAX / (MAX_X - MIN_X)));
    float y = MIN_Y + static_cast<float> (rand()) / (static_cast<float> (RAND_MAX / (MAX_Y - MIN_Y)));
    float z = MIN_Z + static_cast<float> (rand()) / (static_cast<float> (RAND_MAX / (MAX_Z - MIN_Z)));

    x *= (float) Ultrahaptics::Units::centimetres;
    y *= (float) Ultrahaptics::Units::centimetres;
    z *= (float) Ultrahaptics::Units::centimetres;

    return Point3D(x, y, z);
}



//---------------------------------------------------------------------------------------------------------
//
// Serial communication with LED Arduino controller

#define NO_SERIAL -1
#define SERIAL_BAUD 9600
#define SERIAL_PORT "/dev/tty.usbmodem1411" // Also try dev/tty.usbmodem621 or /dev/tty.usbmodem411

/**
 * Turns all LEDs off. `serial_port' should be a file descriptor.
 */
void ledsOff(int serial_port) {
    if (serial_port != -1) {
        serialport_write(serial_port, "3\n"); // 3 is the protocol value for switching all LEDs off
    }
}

/**
 * Sets the LEDs to the specified RGB values.
 */
bool ledsSetRGB(int serial_port, int r, int g, int b) {
    if (serial_port != -1) {
        std::stringstream cmd;
        cmd << "2 " << r << " " << g << " " << b << "\n"; // 2 is the protocol value for updating all LEDs
        std::string s = cmd.str();

        serialport_write(serial_port, s.c_str());

        return true;
    } else {
        return false;
    }
}

/**
 * Interpolates between two values. `pct' is the percentage in range of 0.0 to 1.0
 */
float simple_interpolate(int a, int b, float pct) {
    return a + (b - a) * pct;
}



//---------------------------------------------------------------------------------------------------------
//
// Utility functions for sensor strength.

/**
 * Calculates the Euclidean distance between two vector points.
 */
double distanceBetweenPoints(const Ultrahaptics::Vector3 a, const Ultrahaptics::Vector3 b) {
    const double xDiff = a.x - b.x;
    const double yDiff = a.y - b.y;
    const double zDiff = a.z - b.z;

    return sqrt(pow(xDiff, 2) + pow(yDiff, 2) + pow(zDiff, 2));
}

/**
 * Calculates the sensor strength value using the provided target object and hand position.
 */
double sensorStrength(const Point3D target, const float radius, const Point3D handPosition) {
    double distance = distanceBetweenPoints(target, handPosition);

    return std::max(0.0, 1.0 - (distance / radius));
}



//---------------------------------------------------------------------------------------------------------
//
// Leap Motion code for tracking hands. Palm position is in Ultrahaptics coordinate space.

struct HandData {
    Point3D palm_position;
    bool hand_present;
};

/**
 * This class provides a listener for Leap Motion hand tracking data. The `onFrame' function processes
 * hand tracking updates from the sensor and updates the feedback accordingly. Remember to set the
 * emitter, feedback and target objects before starting the hand tracker.
 */
class LeapListener : public Leap::Listener {
public:
    LeapListener() : current_update(0), atomic_local_hand_data(local_hand_data[0]) {
        current_update++;
        task_started = false;
        hand_entered_on_task = false;
        serial_port = NO_SERIAL;
        current_sensor_strength = 0.0;
    }

    LeapListener(const LeapListener &other) = delete;
    LeapListener& operator=(const LeapListener &other) = delete;

    void onFrame(const Leap::Controller & controller) {
        // Get hand positions from the leap motion controller
        const Leap::Frame frame = controller.frame();
        const Leap::HandList hands = frame.hands();

        if (hands.isEmpty()) {
            local_hand_data[current_update & 1].palm_position = Ultrahaptics::Vector3();
            local_hand_data[current_update & 1].hand_present = false;
            atomic_local_hand_data.store(local_hand_data[current_update & 1]);
            current_update++;

            // If LEDs are enabled, switch LEDs off
            if (serial_port != NO_SERIAL) {
                if (current_sensor_strength != 0.0) {
                    ledsOff(serial_port);
                    current_sensor_strength = 0.0;
                }
            }
        } else {
            // Check if this is the beginning of the task - i.e., hand first seen
            if (!task_started) {
                task_started = true;
                hand_entered_on_task = true;
                task_start_time = std::chrono::high_resolution_clock::now();
            }

            // Get the data for the front-most hand
            const Leap::Hand &hand = hands.frontmost();

            // Convert to Ultrahaptics coordinate space
            const Ultrahaptics::Vector3 leap_palm_position = Ultrahaptics::Vector3(hand.palmPosition().x, hand.palmPosition().y, hand.palmPosition().z);
            const Ultrahaptics::Vector3 uh_palm_position  = alignment.fromTrackingPositionToDevicePosition(leap_palm_position);

            // Update the hand data with the current information
            local_hand_data[current_update & 1].palm_position = uh_palm_position;
            local_hand_data[current_update & 1].hand_present = true;
            atomic_local_hand_data.store(local_hand_data[current_update & 1]);
            current_update++;

            // If LEDs are enabled, calculate a new sensor strength value and update the LEDs
            if (serial_port != NO_SERIAL && !disable_leds) {
                float sensor_strength = (float) fmin(1.0f, sensorStrength(target_point, INTERACTION_SPHERE_RADIUS, uh_palm_position));

                // Boost to widen the target area
                sensor_strength = fminf(1.0, sensor_strength + SWEET_SPOT_STRENGTH_BOOST);

                // Only update if the change in strength exceeds a threshold; stops the serial from saturating
                if (fabs(sensor_strength - current_sensor_strength) >= LED_UPDATE_STEP_THRESHOLD) {
                    current_sensor_strength = sensor_strength;

                    // Interpolate the LED colour values
                    const int r = (int) simple_interpolate(LOW_STRENGTH_R, HIGH_STRENGTH_R, sensor_strength);
                    const int g = (int) simple_interpolate(LOW_STRENGTH_G, HIGH_STRENGTH_G, sensor_strength);
                    const int b = (int) simple_interpolate(LOW_STRENGTH_B, HIGH_STRENGTH_B, sensor_strength);

                    // Set LED values
                    ledsSetRGB(serial_port, r, g, b);
                }
            }
        }

        // Check for keyboard input: [space] to end task, [Q] to quit
        if (keyboardHit()) {
            int key = getchar();

            // Only end task if the hand was detected over the sensor
            if (key == ' ' && hand_entered_on_task) {
                // Check if enough time has elapased between keyboard presses - catch out accidental double press
                std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double, std::milli> time_between_press = now - spacebar_last_pressed;

                if (time_between_press.count() < SPACEBAR_PRESS_THRESHOLD_TIME) {
                    return;
                } else {
                    spacebar_last_pressed = now;
                }

                disable_leds = true;

                // End the current task by notifying the conditional variable. The main function logs the data.
                std::unique_lock<std::mutex> lock(mutex);
                next_task_ready.notify_all();
            } else if (key == 'q') {
                std::cout << std::endl << "Quitting..." << std::endl;
                exit(EXIT_SUCCESS);
            }
        }
    }

    /**
     * Returns the last known palm position, in Ultrahaptics coordinate space.
     */
    const Ultrahaptics::Vector3 getPalmPosition() {
        return (atomic_local_hand_data.load()).palm_position;
    }

    /**
     * Returns true if the hand is known to be currently tracked.
     */
    const bool isHandPresent() {
        return (atomic_local_hand_data.load()).hand_present;
    }

    /**
     * Returns the start time of the current task.
     */
    const std::chrono::high_resolution_clock::time_point getStartTime() {
        return task_start_time;
    }

    /**
     * Resets the local task data.
     */
    const void nextTask(Point3D point) {
        task_started = false;
        hand_entered_on_task = false;
        task_start_time = std::chrono::high_resolution_clock::now();
        target_point = point;
        disable_leds = false;
    }

    /**
     * Returns true if the users hand entered on the last task.
     */
    const bool handEnteredOnTask() {
        return hand_entered_on_task;
    }

    /**
     * Sets the LED serial port to the given file descriptor number.
     */
    const void setLEDPort(int port) {
        serial_port = port;
    }

private:
    std::atomic<HandData> atomic_local_hand_data;
    HandData local_hand_data[2];
    Ultrahaptics::Alignment alignment;
    int current_update;
    std::chrono::high_resolution_clock::time_point spacebar_last_pressed;
    std::chrono::high_resolution_clock::time_point task_start_time;
    bool task_started;
    bool hand_entered_on_task;
    int serial_port;
    double current_sensor_strength;
    Point3D target_point;
    bool disable_leds;
};



//---------------------------------------------------------------------------------------------------------
//
// Ultrahaptics feedback code.

/**
 * The Feedback structure contains values that define the feedback.
 */
struct Feedback {
    LeapListener hand;                // Leap Motion data callback class
    int serial_port;                  // Serial port for Arduino LED control
    bool use_leds;                    // If true, LEDs are used as feedback
    bool use_haptics;                 // If true, Ultrahaptics is used as feedback
    Point3D focal_point;                // Location of the Ultrahaptic focal point
    Point3D target_point;               // Location of the current target point
    float interaction_sphere_radius;  // The radius of the interaction sphere for calculating sensor strength
    float uh_intensity;               // The ultrahaptic intensity value (0.0 or 1.0 depending on `use_haptics')
    float circle_radius;              // Maximum radius of the Ultrahaptic feedback area
    float circle_frequency;           // Circumference traversal rate of the Ultrahaptic feedback area
    bool is_initial_interval;         // Flag used to determine if the initial interval calculations are necessary
    float previous_sample_phase;      // Previous sample phase; used to calculate the next phase in the time point stream
};

/**
 * Prints debug information about the given feedback structure.
 */
void showFeedback(Feedback* feedback) {
    std::cout << std::endl;
    std::cout << "Feedback:" << std::endl;
    std::cout << "  * Intensity:        " << feedback->uh_intensity * 100.0f << "%" << std::endl;
    std::cout << "  * Circle frequency: " << feedback->circle_frequency << "Hz" << std::endl;
    std::cout << "  * Circle radius:    " << feedback->circle_radius * 100.0f << "cm" << std::endl;
    std::cout << "  * Sphere origin:    " << feedback->target_point.x * 100.0f << "cm, " << feedback->target_point.y * 100.0f << "cm, " << feedback->target_point.z * 100.0f << "cm" << std::endl;
    std::cout << "  * Sphere radius:    " << feedback->interaction_sphere_radius * 100.0f << "cm" << std::endl;
    std::cout << std::endl;
}

/**
 * Time-point streaming emitter callback.
 *
 * @param emitter
 * @param interval
 * @param submission_deadline
 * @param user_pointer Contains a pointer to a Feedback structure.
 */
void emitter_callback(const TimePointEmitter &emitter, OutputInterval &interval, const Ultrahaptics::TimePoint &submission_deadline, void *user_pointer) {
    // Cast the user pointer to the struct that describes the feedback
    struct Feedback* feedback = (struct Feedback*) user_pointer;

    // Get the phase angle interval between control point samples
    const float phase_interval = 2 * (float) M_PI * (feedback->circle_frequency / ((float) emitter.getSampleRate()));

    // Initialise the control point sample before the first to be at the start of the very first interval
    if (feedback->is_initial_interval) {
        feedback->is_initial_interval = false;

        // Ensure the next sample phase is zero
        feedback->previous_sample_phase = -phase_interval;
    }

    // Set up the next sample phase by adding the phase interval to the previous phase
    float next_sample_phase = feedback->previous_sample_phase + phase_interval;

    // Loop through the samples in this interval
    for (IntervalIterator it = interval.begin(); it < interval.end(); ++it) {
        if (feedback->hand.isHandPresent()) {
            // Set the haptic circle radius based on depth into the sphere, creating illusion of a 3D sphere
            const float z_distance_from_origin = fabsf(feedback->hand.getPalmPosition().z - feedback->target_point.z);
            const float z_distance_from_origin_pct = fminf(1.0f, z_distance_from_origin / feedback->interaction_sphere_radius);
            const float cross_section_radius = feedback->circle_radius * (1 - z_distance_from_origin_pct);

            // Calculate the new focal point position
            feedback->focal_point.x = feedback->target_point.x + std::cos(next_sample_phase) * cross_section_radius;
            feedback->focal_point.y = feedback->target_point.y + std::sin(next_sample_phase) * cross_section_radius;

            // Move the haptic circle (i.e., cross section of the sphere) to the same height as the hand
            feedback->focal_point.z = feedback->hand.getPalmPosition().z - 3.0f * (float) Ultrahaptics::Units::millimetres;
        }

        // Set the position and intensity of the persistent control point to that of the modulated wave at this point in time
        it->persistentControlPoint(0).setPosition(feedback->focal_point);
        it->persistentControlPoint(0).setIntensity(feedback->hand.isHandPresent() ? feedback->uh_intensity : 0.0f);

        // The current sample phase now becomes the last sample phase
        feedback->previous_sample_phase = next_sample_phase;

        // The next sample phase is determined by adding the phase interval
        next_sample_phase += phase_interval;
    }

    // Reset to remainder of 2PI to prevent floating-point overflow issues
    feedback->previous_sample_phase = std::fmod(feedback->previous_sample_phase, 2.0f * (float) M_PI);
}



//---------------------------------------------------------------------------------------------------------
//
// Main execution code.

/**
 * Prints help info for the runtime options.
 */
void usage(void) {
    printf("Usage: HaptiGlow [OPTIONS]\n"
                   "\n"
                   "Options:\n"
                   "  -h, --help                 Print this help message\n"
                   "  -p, --participant=p_num    Set the participant number for user study\n"
                   "  -b, --block=b_num          Set the block number for user study\n"
                   "  -d, --demo                 Run in demonstration mode\n"
                   "  -l, --leds                 Enable LED feedback in the demo mode\n"
                   "  -u  --haptics              Enable Ultrahaptic feedback in the demo mode\n"
                   "\n");
    exit(EXIT_SUCCESS);
}

/**
 * Initialises feedback and establishes connections with the Leap Motion controller and the Ultrahaptics device.
 */
int main(int argc, char *argv[]) {
    // Parse runtime options
    int option_index = 0;
    int opt;
    static struct option loptions[] = {
            {"help",        no_argument,       0, 'h'},
            {"participant", required_argument, 0, 'p'},
            {"block",       required_argument, 0, 'b'},
            {"demo",        no_argument,       0, 'd'},
            {"leds",        no_argument,       0, 'l'},
            {"haptics",     no_argument,       0, 'u'},
            {NULL,          0,                 0, 0}
    };

    int participant = -1;
    int block = -1;
    int condition;
    int** latin_square = getLatinSquare(N_BLOCKS);
    bool demo_mode = false;
    bool use_haptics;
    bool use_leds;

    while (true) {
        opt = getopt_long(argc, argv, "hdlup:b:", loptions, &option_index);

        if (opt == -1) {
            break;
        }

        switch (opt) {
            case 'h':
                usage();
                break;
            case 'b':
                block = std::stoi(optarg);
                break;
            case 'p':
                participant = std::stoi(optarg);
                break;
            case 'd':
                demo_mode = true;
                break;
            default:
                break;
        }
    }

    // For demo mode, just use a really high participant and block number
    if (demo_mode) {
        participant = DEMO_PARTICIPANT;
        block = DEMO_BLOCK;
    }

    // Determine which mode to run under
    if (participant < 1 || block < 1 || (block != DEMO_BLOCK && block > N_BLOCKS)) {
        std::cout << "Error: Participant number and block number must be specified and correct." << std::endl << std::endl;
        usage();
        exit(EXIT_FAILURE);
    } else {
        // Determine which feedback types to use for this condition
        condition = latin_square[participant % N_BLOCKS][block - 1];
        use_haptics = (condition % 3) + 1 == CONDITION_HAPTIC || (condition % 3) + 1 == CONDITION_MULTI;
        use_leds = (condition % 3) + 1 == CONDITION_VISUAL || (condition % 3) + 1 == CONDITION_MULTI;

        std::cout << "Participant: " << participant << std::endl;
        std::cout << "Block:       " << block << std::endl;
        std::cout << "Condition:   " << condition << " [" << getConditionName(condition) << "]" << std::endl << std::endl;
    }

    // Initialise Leap Motion
    Leap::Controller *controller = new Leap::Controller();
    controller->setPolicyFlags(Leap::Controller::PolicyFlag::POLICY_BACKGROUND_FRAMES);

    // Attempt to connect to Leap Motion; if it fails after 5 attempts, quit
    int attempts = 0;
    while (!controller->isConnected()) {
        std::cout << "Leap Motion: Controller not connected! Sleeping for 300ms." << std::endl;

        attempts++;
        std::this_thread::sleep_for(std::chrono::milliseconds(300));

        if (attempts == 5) {
            std::cout << "Leap Motion: Device not found. Quitting..." << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    // Create a feedback structure
    Feedback feedback;
    feedback.interaction_sphere_radius = INTERACTION_SPHERE_RADIUS;
    feedback.focal_point = Point3D(feedback.target_point.x, feedback.target_point.y, feedback.target_point.z);
    feedback.uh_intensity = use_haptics ? 1.0f : 0.0f; // If not using haptics, set intensity to 0%
    feedback.circle_radius = CONE_MAX_RADIUS;
    feedback.circle_frequency = CONE_TRAVERSAL_FREQUENCY;
    feedback.is_initial_interval = true;
    feedback.use_leds = use_leds;
    feedback.use_haptics = use_haptics;
    showFeedback(&feedback);

    // Tell the Leap Motion controller to use the feedback structure callback
    controller->addListener(feedback.hand);

    // Initialise serial port
    int serial_port = -1;
    if (feedback.use_leds) {
        serial_port = serialport_init(SERIAL_PORT, SERIAL_BAUD);

        if (serial_port == -1) {
            std::cout << "Unable to open serial port " << SERIAL_PORT << std::endl;
            exit(EXIT_FAILURE);
        } else {
            feedback.hand.setLEDPort(serial_port);
            serialport_flush(serial_port);
            ledsOff(serial_port);
        }

        feedback.serial_port = serial_port;
    }

    // Initialise Ultrahaptics device regardless of whether or not we use it
    TimePointEmitter emitter;
    emitter.setMaximumControlPointCount(1);
    emitter.setEmissionCallback(emitter_callback, &feedback);
    emitter.start();

    if (!demo_mode) {
        std::cout << "Starting experiment..." << std::endl << std::endl;

        // Main experiment loop
        for (int task = 1; task <= N_TASKS_PER_BLOCK; task++) {
            // Wait for the participant to take their hand away
            std::cout << std::endl << std::endl;
            while (feedback.hand.isHandPresent()) {
                std::cout << "Please place your hands on the desk" << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }

            // Get random target point and initialise timing data
            feedback.target_point = getRandomPoint();
            feedback.hand.nextTask(feedback.target_point);

            // Enable Ultrahaptic feedback again, if necessary
            if (feedback.use_haptics) {
                feedback.uh_intensity = 1.0f;
            }

            // Output task metadata and instructions
            std::cout << std::endl;
            std::cout << "Task " << task << " of " << N_TASKS_PER_BLOCK << std::endl;
            std::cout << "Press the button when you have completed the task..." << std::endl;

            // Wait until task is complete; the Leap Motion onFrame function will check for keyboard input and notify when finished
            std::unique_lock<std::mutex> lock(mutex);
            next_task_ready.wait(lock);

            // Make sure the LEDs are off
            if (feedback.use_leds) {
                ledsOff(feedback.serial_port);
            }

            // Turn off the Ultrahaptic feedback
            if (feedback.use_haptics) {
                feedback.uh_intensity = 0.0f;
            }

            // Calculate the time for the task
            std::chrono::high_resolution_clock::time_point end_time = std::chrono::high_resolution_clock::now();
            double task_time;

            if (feedback.hand.handEnteredOnTask()) {
                std::chrono::duration<double, std::milli> milliseconds = end_time - feedback.hand.getStartTime();
                task_time = milliseconds.count();
            } else {
                task_time = -1.0;
            }

            // Log the results of the previous task before starting the next one
            Point3D hand_position = feedback.hand.getPalmPosition();
            double distance = distanceBetweenPoints(feedback.target_point, hand_position);
            double strength = sensorStrength(feedback.target_point, INTERACTION_SPHERE_RADIUS, hand_position);

            std::string log_message = getLogLine(participant, block, condition, task, task_time, distance, feedback.target_point, hand_position, strength);
            appendLineToFile(getLogName(participant, block), log_message);

            // Print message and play a short audio tone
            std::cout << std::endl << "Finished. Waiting for next task..." << std::endl << "\a\a\a";

            // Sleep for a bit after the task ends
            std::this_thread::sleep_for(std::chrono::milliseconds(PAUSE_BETWEEN_TASKS));
        }
    } else {
        std::cout << "Demo mode started..." << std::endl;

        // Wait for enter key to be pressed.
        std::cout << "Press [Enter] to quit..." << std::endl;
        std::string line;
        std::getline(std::cin, line);
    }

    // Stop any feedback from the array and disconnect from Leap Motion
    std::cout << "Quitting..." << std::endl;
    controller->removeListener(feedback.hand);

    if (feedback.use_haptics) {
        emitter.stop();
    }

    if (feedback.use_leds) {
        ledsOff(serial_port);
        serialport_flush(serial_port);
        serialport_close(serial_port);
    }

    return EXIT_SUCCESS;
}
