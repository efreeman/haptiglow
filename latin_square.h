//
// Created by Euan Freeman on 16/08/2017.
//

#ifndef HAPTIGLOW_LATIN_SQUARE_H
#define HAPTIGLOW_LATIN_SQUARE_H
int** getLatinSquare(int n);
void printLatinSquare(int** square, int n);
#endif //HAPTIGLOW_LATIN_SQUARE_H
