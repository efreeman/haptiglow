//
// Created by Euan Freeman on 16/08/2017.
//

#ifndef HAPTIGLOW_LOGGER_H
#define HAPTIGLOW_LOGGER_H
void appendLineToFile(std::string filepath, std::string line);
#endif //HAPTIGLOW_LOGGER_H
