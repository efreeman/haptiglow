//
// Created by Euan Freeman on 16/08/2017.
//

#include <fstream>
#include "logger.h"

/**
 * Append the given line to the specified file.
 */
void appendLineToFile(std::string filepath, std::string line) {
    std::ofstream file(filepath, std::ios::out | std::ios::app);

    if (file.fail()) {
        throw std::ios_base::failure(std::strerror(errno));
    }

    // Make sure write fails with exception if something is wrong
    file.exceptions(file.exceptions() | std::ios::failbit | std::ifstream::badbit);

    file << line;
}