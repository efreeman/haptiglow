//
// Created by Euan Freeman on 16/08/2017.
//

#include <iostream>
#include "latin_square.h"

int** getLatinSquare(const int n) {
    // 1. Create table
    int** latinSquare = new int*[n];

    for (int i = 0; i < n; ++i) {
        latinSquare[i] = new int[n];
    }

    // 2. Init first row
    latinSquare[0][0] = 1;
    latinSquare[0][1] = 2;

    for (int i = 2, j = 3, k = 0; i < n; i++) {
        if (i % 2 == 1)
            latinSquare[0][i] = j++;
        else
            latinSquare[0][i] = n - (k++);
    }

    // 3. Init first column
    for (int i = 1; i <= n; i++) {
        latinSquare[i - 1][0] = i;
    }

    // 4. Complete table
    for (int row = 1; row < n; row++) {
        for (int col = 1; col < n; col++) {
            latinSquare[row][col] = (latinSquare[row - 1][col] + 1) % n;

            if (latinSquare[row][col] == 0) {
                latinSquare[row][col] = n;
            }
        }
    }

    return latinSquare;
}

void printLatinSquare(int** square, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            std::cout << square[i][j] << " ";
        }

        std::cout << std::endl;
    }
}