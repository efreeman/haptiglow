//
// Created by Euan Freeman on 16/08/2017.
//

#include "keyboard.h"

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdio>

/**
 * Returns true if a key is currently pressed on the keyboard. When true, the getchar() function can be
 * used in a non-blocking way to get the pressed key ID.
 */
bool keyboardHit(void) {
    struct termios oldt, newt;
    int ch;
    int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);

    if (ch != EOF) {
        ungetc(ch, stdin);
        return true;
    }

    return false;
}