# Find Ultrahaptics
#
# Finds the libraries and header files for the Ultrahaptics Evaluation SDK for
# the Ultrahaptics device.
#
# This module defines
# ULTRAHAPTICS_FOUND       - Ultrahaptics library and headers were found
# ULTRAHAPTICS_INCLUDE_DIR - Directory containing Ultrahaptics header files
# ULTRAHAPTICS_LIBRARY     - Library name of the Ultrahaptics library

SET ( WINDOWS_LOCATION_ULTRAHAPTICS_DIR )
IF (WIN32)
  # 32-bit
  IF ( CMAKE_SIZEOF_VOID_P EQUAL 4 )
    SET ( UNINSTALL_LOCATION [HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\ultrahaptics;UninstallString])
    IF ( UNINSTALL_LOCATION )
      GET_FILENAME_COMPONENT ( WINDOWS_LOCATION_ULTRAHAPTICS_DIR "${UNINSTALL_LOCATION}" DIRECTORY )
    ELSE ( UNINSTALL_LOCATION )
      SET ( UNINSTALL_LOCATION [HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\ultrahaptics;UninstallString])
      IF ( UNINSTALL_LOCATION )
        GET_FILENAME_COMPONENT ( WINDOWS_LOCATION_ULTRAHAPTICS_DIR "${UNINSTALL_LOCATION}" DIRECTORY )
      ENDIF ( UNINSTALL_LOCATION )
    ENDIF ( UNINSTALL_LOCATION )
  ENDIF ( CMAKE_SIZEOF_VOID_P EQUAL 4 )
  # 64-bit
  IF ( CMAKE_SIZEOF_VOID_P EQUAL 8 )
    SET ( UNINSTALL_LOCATION [HKEY_LOCAL_MACHINE\\SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\ultrahaptics-64;UninstallString])
    IF ( UNINSTALL_LOCATION )
      GET_FILENAME_COMPONENT ( WINDOWS_LOCATION_ULTRAHAPTICS_DIR "${UNINSTALL_LOCATION}" DIRECTORY )
    ENDIF ( UNINSTALL_LOCATION )
  ENDIF ( CMAKE_SIZEOF_VOID_P EQUAL 8 )
ENDIF (WIN32)

FIND_PATH (
   ULTRAHAPTICS_INCLUDE_DIR
   NAMES
      Ultrahaptics.hpp
   HINTS
      $ENV{ULTRAHAPTICSINCLUDEDIR}
      ~/Library/Frameworks
      /Library/Frameworks
      /usr/include
      /usr/local/include
      /sw/include # Fink
      /opt/local/include # DarwinPorts
      /opt/csw/include # Blastwave
      /opt/include
      ${WINDOWS_LOCATION_ULTRAHAPTICS_DIR}/include
   PATH_SUFFIXES ultrahaptics Ultrahaptics
)

FIND_LIBRARY (
   ULTRAHAPTICS_LIBRARY
   NAMES
      Ultrahaptics
      libUltrahaptics
   HINTS
      $ENV{ULTRAHAPTICSDIR}
      ~/Library/Frameworks
      /Library/Frameworks
      /usr/lib
      /usr/lib64
      /usr/local/lib
      /usr/local/lib64
      /sw # Fink
      /opt/local # DarwinPorts
      /opt/csw # Blastwave
      /opt
      /opt/local/lib
      /opt/local/lib64
      /opt/lib
      /opt/lib64
      ${WINDOWS_LOCATION_ULTRAHAPTICS_DIR}/lib
      ${ULTRAHAPTICS_HINTS}
)

FIND_LIBRARY (
   ULTRAHAPTICS_COMMON_LIBRARY
   NAMES
      UltrahapticsCommon
      libUltrahapticsCommon
   HINTS
      $ENV{ULTRAHAPTICSDIR}
      ~/Library/Frameworks
      /Library/Frameworks
      /usr/lib
      /usr/lib64
      /usr/local/lib
      /usr/local/lib64
      /sw # Fink
      /opt/local # DarwinPorts
      /opt/csw # Blastwave
      /opt
      /opt/local/lib
      /opt/local/lib64
      /opt/lib
      /opt/lib64
      ${WINDOWS_LOCATION_ULTRAHAPTICS_DIR}/lib
      ${ULTRAHAPTICS_HINTS}
)

SET ( ULTRAHAPTICS_LIBRARIES ${ULTRAHAPTICS_LIBRARY} ${ULTRAHAPTICS_COMMON_LIBRARY} )
SET ( ULTRAHAPTICS_INCLUDE_DIRS ${ULTRAHAPTICS_INCLUDE_DIR} )

INCLUDE ( FindPackageHandleStandardArgs )

FIND_PACKAGE_HANDLE_STANDARD_ARGS(Ultrahaptics
   DEFAULT_MSG ULTRAHAPTICS_LIBRARIES ULTRAHAPTICS_INCLUDE_DIR)

MARK_AS_ADVANCED ( 
   ULTRAHAPTICS_INCLUDE_DIR
   ULTRAHAPTICS_LIBRARIES
   ULTRAHAPTICS_LIBRARY
)
